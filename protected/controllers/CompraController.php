<?php

class CompraController extends Controller
{
	public function actionCreate()
	{
      $model = new Compra;
		$this->render('create',array('model'=>$model));
	}

   public function actionAddProducto( )
	{
      $item = $_GET['item'];
		$this->renderPartial('itemProducto',array('item'=>$item));
	}


   public function actionSave( )
   {
      
   }

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionView()
	{
		$this->render('view');
	}
}
