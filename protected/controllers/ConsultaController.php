<?php

class ConsultaController extends Controller
{
	public function actionCompras()
	{
		$this->render('compras');
	}

	public function actionGraficas()
	{
		$this->render('graficas');
	}

	public function actionOrdenes()
	{
		$this->render('ordenes');
	}

   public function actionFiltroOrdenes( )
   {
      $fecha_desde = myCHtml::date_mysql($_POST['fecha_desde']);
      $fecha_hasta = myCHtml::date_mysql($_POST['fecha_hasta']);
      $criteria = new CDbCriteria( ); 
      $criteria->addBetweenCondition('fecha_emision',$fecha_desde,$fecha_hasta);
      $model = OrdenCompra::model( )->findAll($criteria);
      $this->renderPartial('_orden_detalle',array('model'=>$model));
   }
   
   public function actionFiltroCompras( )
   {
      $fecha_desde = myCHtml::date_mysql($_POST['fecha_desde']);
      $fecha_hasta = myCHtml::date_mysql($_POST['fecha_hasta']);
      $criteria = new CDbCriteria( ); 
      $criteria->addBetweenCondition('fecha_emision',$fecha_desde,$fecha_hasta);
      $model = Compra::model( )->findAll($criteria);
      $this->renderPartial('_compras_detalle',array('model'=>$model));
   }


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
