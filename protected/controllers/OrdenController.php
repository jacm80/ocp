<?php

class OrdenController extends Controller
{
	static $_item=0;

	public function actionIndex( )
	{
		$model = new OrdenCompra('search');
		$model->unsetAttributes( );
		if (isset($_GET['OrdenCompra']))
		{
		  $model->setAttributes($_GET['OrdenCompra']);
		}
		$this->render('index',array('model'=>$model));
	}

	public function actionView($id)
	{
		$this->render('view',array('model'=>$this->loadModel($id)));
	}

	public function actionAddProducto( )
	{
      $item = $_GET['item'];
		$this->renderPartial('itemProducto',array('item'=>$item));
	}

	public function actionCreate()
	{
		$model = new OrdenCompra;
		$this->render('create',array('model'=>$model));
	}

   public function loadModel($id)
	{
		$model = OrdenCompra::model( )->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

   public function actionImprimir($id)
   {
      $id    = $_GET['id'];
      $model = $this->loadModel($id);
      //$this->renderPartial('_orden', array('model'=>$model));
      /**/
      $cssPrint = file_get_contents(getcwd().'/media/css/ordencompra.css');
      $mPDF1 = Yii::app( )->ePdf->mpdf('','A4',0,15,15,15,15,8,8);
      //$mPDF1->SetMargins(2,2,2);
      $mPDF1->WriteHTML($cssPrint,1);
      $mPDF1->WriteHTML($this->renderPartial('_orden', array('model'=>$model),true));
      $mPDF1->Output( );
      /**/
   }

	public function actionGuardar()
	{
		$orden = new OrdenCompra;
		// guardar encabezado de la orden
      $orden->proveedor_id   = $_POST['Orden']['proveedor_id' ];
		$orden->fecha_emision  = myCHtml::date_mysql($_POST['Orden']['fecha_emision']);
		$orden->fecha_registro = date('Y-m-d');
		$i=0;
		$orden->save( );
      // guardar el detalle de la orden de compra
      /////////////////////////////////////////////////////////////////////////
		foreach ($_POST['OrdenItem']['id'] as $id) 
		{
			$o = new OrdenItem;
			$o->cantidad 		   = $_POST['OrdenItem']['cantidad'][$i];
			$o->precio_unitario  = $_POST['OrdenItem']['precio_unitario'][$i];
			$o->iva = ($o->cantidad * $o->precio_unitario) * 0.12;
			$o->orden_compra_id	= $orden->id;
			$o->producto_id 	   = $_POST['OrdenItem']['id'][$i];
			$o->save( );
			$i++;
		}
      /////////////////////////////////////////////////////////////////////////
	   $this->redirect(array('Imprimir','id'=>$orden->id));      
	}

}
