<?php
   
   class myCHtml extends CHtml {
      
      public static function loadjQuery( )
      {
         Yii::app()->clientScript->registerCoreScript('jquery');
      }

      public static function loadjQueryUI( )
      {
         Yii::app()->clientScript->registerCoreScript('jquery.ui');
         Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');
      }

      public static function imageUrl($name)
      {
         return Yii::app()->baseUrl.'/media/images/'.$name;
      }
      
      public static function loadCss($name)
      {
         if (Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/media/css/'.$name.'.css')) return TRUE;
      }
      
      public static function loadJs($name)
      {

         if (Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/media/js/'.$name.'.js')) return TRUE;
      }
      public static function debug($var)
      {
         CVarDumper::dump($var,10,true);
      }

      public static function date_mysql($date)
		{
			if (preg_match("/^(\d{2})\/(\d{2})\/(\d{4})$/",$date,$m))
			{
				return $m[3].'-'.$m[1].'-'.$m[2];
			}
			else { return 'error'; }
		}

   }
?>
