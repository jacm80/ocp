<?php

   return array(
      'Home'=>'Inicio',
      'About'=>'Acerca de...',
      'File'=>'Archivo',
      'Customer'=>'Clientes',
      'Contact'=>'Contacto',
      'Delete'=>'Eliminar',
      'Update'=>'Actualizar',
      'List'=>'Listado',
      'Delete'=>'Eliminar',
      'View'=>'Ver',
      'Manage'=>'Administrar',
      'Create'=>'Crear',
      'Name'=>'Nombre',
      'Save'=>'Guardar',
      'Save # {VAR}'=>'Guardar el numero {VAR}'
   );

?>
