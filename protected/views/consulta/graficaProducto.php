<?php
/* @var $this ConsultaController */

$this->breadcrumbs=array(
	'Consulta'=>array('/consulta'),
	'Graficas',
);
?>
<h1 style="text-align:center;">Tendencia de Productos Comprados</h1>

<div style="text-align:center;">
<?php 
      $this->widget(
            'chartjs.widgets.ChLine', 
            array(
                'width' => 600,
                'height' => 300,
                'htmlOptions' => array(),
                'labels' => array_keys($resumen),
                'datasets' => array(
                    array(
                        "fillColor" => "#e3e3e3",
                        "strokeColor" => "rgba(0,220,220,1)",
                        "data" => array_values($resumen)
                    )       
                ),
                'options' => array()
            )
      ); 
?>
</div>
