<style>
   h1 {
      text-align:center;
   }
   table {
      border: 1px solid #e3e3e3;
      width: 80%;
      margin: 0 auto;
   }
   tr td,th  {
      border: 1px solid #e3e3e3;
   }
</style>

<script type="text/javascript">
   $(document).ready(function(){
      $('#BtnConsultar').click(function(){
         $.ajax({
                  url: 'http://localhost/ocp/index.php?r=consulta/filtroCompras',
			         async: true,
			         type:'post',
                  data: { 'fecha_desde': $('#fecha_desde').val(), 'fecha_hasta':$('#fecha_hasta').val( ) },
			         dataType: 'html',
			         success: function(response){
				         $('#detalle').html(response);
		      	   },
			         error: function(){ alert('Error en el servidor'); }
               });
      });
   });
</script>

<?php
/* @var $this ConsultaController */
$this->breadcrumbs=array(
	'Consulta'=>array('/consulta'),
	'Ordenes',
);
?>
<h1>Compras Registradas</h1>

<table>
   <tr>
      <td>Fecha Desde</td>
      <td>
         <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    								 'name' =>'fecha_desde',
    								 //'value'=> myCHtml::to_ddmmyyyy($model->fecha_emision),
    								 // additional javascript options for the date picker plugin
    								 'options'    =>array(
                                                'showAnim'=>'fold',
                                                'currentText'=>'Now',
                                                'dateFormat'=>'dd/mm/yy',
                                                ),
    								 'htmlOptions'=>array('style'=>'height:20px;')));?>
      </td>
      <td>Fecha Hasta</td>
      <td>
         <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    								 'name' =>'fecha_hasta',
    								 //'value'=> myCHtml::to_ddmmyyyy($model->fecha_emision),
    								 // additional javascript options for the date picker plugin
    								 'options'    =>array(
                                                'showAnim'=>'fold',
                                                'currentText'=>'Now',
                                                'dateFormat'=>'dd/mm/yy',
                                                ),
    								 'htmlOptions'=>array('style'=>'height:20px;')));?>

      </td>
   </tr>
</table>
<br />
<div style="text-align:center">
   <button type="button" id="BtnConsultar">Consultar</button>
</div>
<br />
<div id="detalle"></div>
