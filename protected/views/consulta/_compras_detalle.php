<table>
   <thead>
      <tr>
         <th>No.</th>
         <th>Fecha</th>
         <th>Proveedor</th>
         <th>Monto</th>
         <th>&nbsp;</th>
      </tr>
   </thead>
   <tbody>
      <?php $total=0.0; ?>
      <?php foreach($model as $o): ?>
      <tr>
         <td><?php echo $o->id; ?></td>
         <td><?php echo $o->fecha_emision; ?></td>
         <td><?php echo $o->proveedor->razon_social; ?></td>
         <td><?php echo $o->base_imponible + $o->iva; ?></td>
         <td><a href="<?php echo Yii::app()->request->baseUrl?>/index.php?r=compra/view/id/<?php echo$o->id; ?>">Ver</a></td>
         <?php $total+=$o->base_imponible; ?>
      </tr>
      <?php endforeach; ?>
   </tbody>
   <tfoot>
      <tr>
         <td colspan="4">Monto Total</td>
         <td><?php echo number_format($total,'2',',','.')?></td>
      </tr>
   </tfoot>
</table>
