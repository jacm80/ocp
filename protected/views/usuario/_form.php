<?php
/* @var $this UsuarioController */
/* @var $model Usuario */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usuario-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

   <div class="row">
		<?php echo $form->labelEx($model,'Confirmar password'); ?>
		<?php echo $form->passwordField($model,'confirm_password',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'fecha_alta'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    								 'name'=>'Usuario[fecha_alta]',
    								 // additional javascript options for the date picker plugin
    								 'options'=>array('showAnim'=>'fold'),
    								 'htmlOptions'=>array('style'=>'height:20px;')));?>
		<?php echo $form->error($model,'fecha_alta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'activo'); ?>
		<?php echo $form->checkBox($model,'activo'); ?>
		<?php echo $form->error($model,'activo'); ?>
	</div>

	<div class="row">
      <?php $grupos = CHtml::listData(GrupoUsuario::model()->findAll(),'id','descripcion'); ?>
		<?php echo $form->labelEx($model,'grupo_usuario_id'); ?>
		<?php echo $form->dropDownList($model,'grupo_usuario_id',$grupos,array('prompt'=>'Selecione el grupo:')); ?>
		<?php echo $form->error($model,'grupo_usuario_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
