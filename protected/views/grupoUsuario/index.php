<?php
/* @var $this GrupoUsuarioController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Grupo Usuarios',
);

$this->menu=array(
	array('label'=>'Create GrupoUsuario', 'url'=>array('create')),
	array('label'=>'Manage GrupoUsuario', 'url'=>array('admin')),
);
?>

<h1>Grupo Usuarios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
