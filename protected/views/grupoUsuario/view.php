<?php
/* @var $this GrupoUsuarioController */
/* @var $model GrupoUsuario */

$this->breadcrumbs=array(
	'Grupo Usuarios'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List GrupoUsuario', 'url'=>array('index')),
	array('label'=>'Create GrupoUsuario', 'url'=>array('create')),
	array('label'=>'Update GrupoUsuario', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GrupoUsuario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GrupoUsuario', 'url'=>array('admin')),
);
?>

<h1>View GrupoUsuario #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
	),
)); ?>
