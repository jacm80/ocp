<?php
/* @var $this GrupoUsuarioController */
/* @var $model GrupoUsuario */

$this->breadcrumbs=array(
	'Grupo Usuarios'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GrupoUsuario', 'url'=>array('index')),
	array('label'=>'Create GrupoUsuario', 'url'=>array('create')),
	array('label'=>'View GrupoUsuario', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage GrupoUsuario', 'url'=>array('admin')),
);
?>

<h1>Update GrupoUsuario <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>