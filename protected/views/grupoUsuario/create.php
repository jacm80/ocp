<?php
/* @var $this GrupoUsuarioController */
/* @var $model GrupoUsuario */

$this->breadcrumbs=array(
	'Grupo Usuarios'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GrupoUsuario', 'url'=>array('index')),
	array('label'=>'Manage GrupoUsuario', 'url'=>array('admin')),
);
?>

<h1>Create GrupoUsuario</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>