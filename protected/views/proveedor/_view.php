<?php
/* @var $this ProveedorController */
/* @var $data Proveedor */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('razon_social')); ?>:</b>
	<?php echo CHtml::encode($data->razon_social); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('domicilio_fiscal')); ?>:</b>
	<?php echo CHtml::encode($data->domicilio_fiscal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('representante')); ?>:</b>
	<?php echo CHtml::encode($data->representante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_fijo')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_fijo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_movil')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_movil); ?>
	<br />


</div>