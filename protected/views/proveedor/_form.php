<?php
/* @var $this ProveedorController */
/* @var $model Proveedor */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proveedor-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'razon_social'); ?>
		<?php echo $form->textArea($model,'razon_social',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'razon_social'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'domicilio_fiscal'); ?>
		<?php echo $form->textArea($model,'domicilio_fiscal',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'domicilio_fiscal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'representante'); ?>
		<?php echo $form->textArea($model,'representante',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'representante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_fijo'); ?>
		<?php echo $form->textField($model,'telefono_fijo',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'telefono_fijo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_movil'); ?>
		<?php echo $form->textField($model,'telefono_movil',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'telefono_movil'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->