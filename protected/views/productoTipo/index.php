<?php
/* @var $this ProductoTipoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Producto Tipos',
);

$this->menu=array(
	array('label'=>'Create ProductoTipo', 'url'=>array('create')),
	array('label'=>'Manage ProductoTipo', 'url'=>array('admin')),
);
?>

<h1>Producto Tipos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
