<?php
/* @var $this ProductoTipoController */
/* @var $model ProductoTipo */

$this->breadcrumbs=array(
	'Producto Tipos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProductoTipo', 'url'=>array('index')),
	array('label'=>'Create ProductoTipo', 'url'=>array('create')),
	array('label'=>'Update ProductoTipo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProductoTipo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProductoTipo', 'url'=>array('admin')),
);
?>

<h1>View ProductoTipo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
	),
)); ?>
