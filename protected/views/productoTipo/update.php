<?php
/* @var $this ProductoTipoController */
/* @var $model ProductoTipo */

$this->breadcrumbs=array(
	'Producto Tipos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProductoTipo', 'url'=>array('index')),
	array('label'=>'Create ProductoTipo', 'url'=>array('create')),
	array('label'=>'View ProductoTipo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProductoTipo', 'url'=>array('admin')),
);
?>

<h1>Update ProductoTipo <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>