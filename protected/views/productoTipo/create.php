<?php
/* @var $this ProductoTipoController */
/* @var $model ProductoTipo */

$this->breadcrumbs=array(
	'Producto Tipos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProductoTipo', 'url'=>array('index')),
	array('label'=>'Manage ProductoTipo', 'url'=>array('admin')),
);
?>

<h1>Create ProductoTipo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>