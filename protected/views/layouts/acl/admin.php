<?php 
   return array(
				      array('label'=>'Home', 'url'=>array('/site/index')),
				      array('label'=>'Orden Compra', 'url'=>array('/orden/create')),
				      array('label'=>'Registrar Factura', 'url'=>array('/compra/create')),
				      array('label'=>'Archivo', 'url'=>'#','items'=>array(
			                           array('label'=>'Proveedor','url'=>array('/proveedor')),
				                        array('label'=>'Tipo de Producto', 'url'=>array('/productoTipo')),
				                        array('label'=>'Producto', 'url'=>array('/producto')),
				                        array('label'=>'Unidades', 'url'=>array('/unidad')),
                                    ),
                        ),
				      array('label'=>'Consulta','url'=>'#','items'=>array(
			                           array('label'=>'Ordenes de Compra','url'=>array('/consulta/ordenes')),
			                           array('label'=>'Compras', 'url'=>array('/consulta/compras')),
                           )),
				      array('label'=>'Administracion','url'=>'#','items'=>array(
			                           array('label'=>'Usuarios','url'=>array('/usuario')),
			                           array('label'=>'Grupos', 'url'=>array('/grupoUsuario')),
                           )),
				      array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				      array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			      ); 

?>
