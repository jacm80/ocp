<?php
/* @var $this FormaPagoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Forma Pagos',
);

$this->menu=array(
	array('label'=>'Create FormaPago', 'url'=>array('create')),
	array('label'=>'Manage FormaPago', 'url'=>array('admin')),
);
?>

<h1>Forma Pagos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
