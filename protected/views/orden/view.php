<?php
	myCHtml::loadjQueryUI( );
	myCHtml::loadJs('orden'); 
?>
<style>
	h1 {
		margin: 0 auto;
		text-align:center;
	}
	table 
	{
		border: 1px solid black;
		width: 80%;
		margin: 0 auto;
	}
	th {
		background-color: #353877 !important;
		color: white;
	}
	th,td {
		border: 1px solid #e3e3e3;
	}
</style>

<?php
/* @var $this OrdenController */
$this->breadcrumbs=array('Orden'=>array('/orden'),'Ver',);?>

<form id="FrmOrden" action="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=orden/update" method="post">
<table>
	<tr>
		<td>Proveedor</td>
		<td><?php echo $model->proveedor->razon_social; ?></td>
		<td>Fecha</td>
		<td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    								 'name'=>'Orden[fecha_emision]',
    								 'value'=>$model->fecha_emision,
    								 // additional javascript options for the date picker plugin
    								 'options'=>array('showAnim'=>'fold'),
    								 'htmlOptions'=>array('style'=>'height:20px;')));?>
    	</td>
	</tr>
</table>
<br>
<table>
	<thead>
		<th>Producto</th>
		<th>Cantidad</th>
		<th>Monto</th>
		<th>&nbsp;</th>
	</thead>
	<tbody id="productos">
		<?php foreach($model->ordenItems as $oi): ?>
			<tr>
				<td>
					<?php echo myCHtml::hiddenField("OrdenItem_{$oi->id}"); ?>
					<?php echo $oi->producto->descripcion; ?>
				</td>
				<td><?php echo $oi->cantidad; ?></td>
				<td><?php echo $oi->precio_unitario; ?></td>
				<td><a href="#">Eliminar</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4" style="text-align:center;">
				<button id="addProducto" type="button">Agregar Producto</button>
				<button id="guardarOrden" type="button">Guardar Orden</button>
				<button id="listarOrdenes" type="button">Listar Ordenes</button>
			</td>
		</tr>
	</tfoot>
</table>
</form>