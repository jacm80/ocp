<?php
/* @var $this OrdenController */

$this->breadcrumbs=array(
	'Orden',
);
?>
<h1>Lista de Ordenes</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orden-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'fecha_emision',
		'estatus',
		array(
			'type'	 => 'raw',
			'name'   => 'proveedor_id',
			'header' => 'Proveedor',
			'value'  => '$data->proveedor->razon_social'
		)
	),
	'selectionChanged'=>'js:function(id){
                                          var xid = $.fn.yiiGridView.getSelection(id);
                                          location.href = "http://localhost/ocp/index.php?r=orden/view&id="+xid;
                                        }'

	,
)); ?>
