<?php
	//myCHtml::loadjQueryUI( );
	myCHtml::loadJs('orden'); 
?>
<style>
	h1 {
		margin: 0 auto;
		text-align:center;
	}
	table 
	{
		border: 1px solid black;
		width: 80%;
		margin: 0 auto;
	}
	th {
		background-color: #353877 !important;
		color: white;
	}
	th,td {
		border: 1px solid #e3e3e3;
	}
	tfoot tr td { text-align:right;} 
</style>

<?php
/* @var $this OrdenController */
$this->breadcrumbs=array('Orden'=>array('/orden'),'Create',);
?>


<!-- <div id="dialog"></div> -->
<h1>Nueva  Orden de Compra</h1>
<br>

<form id="FrmOrden" action="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=orden/guardar" method="post">
<table>
	<tr>
		<td>Proveedor</td>
		<td>
			<?php echo CHtml::dropDownList('Orden[proveedor_id]',0,Proveedor::getProveedores( ),array()); ?>
		</td>
		<td>Fecha</td>
		<td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    								 'name'=>'Orden[fecha_emision]',
    								 // additional javascript options for the date picker plugin
    								 'options'=>array('showAnim'=>'fold'),
    								 'htmlOptions'=>array('style'=>'height:20px;')));?>
    	</td>
	</tr>
</table>
<br>
<table>
	<thead>
		<th>Producto</th>
		<th>Cantidad</th>
		<th>Precio</th>
		<th>Monto</th>
		<th>&nbsp;</th>
	</thead>
	<tbody id="productos"></tbody>
	<tfoot>
		<tr>
			<td colspan="4">SubTotal</td>
			<td><div id="monto">0,00</div></td>
		</tr>
		<tr>
			<td colspan="4">IVA</td>
			<td><div id="iva">0,00</div></td>
		</tr>
		<tr>
			<td colspan="4">Total a Pagar</td>
			<td><div id="total">0,00</div></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align:center;">
				<button id="addProducto" type="button">Agregar Producto</button>
				<button id="guardarOrden" type="button">Guardar Orden</button>
				<button id="listarOrdenes" type="button">Listar Ordenes</button>
			</td>
		</tr>
	</tfoot>
</table>
</form>
