<html>
   <head>
      <title>Orden de Compra</title>
   </head>
   <body>
      <div class="encabezado_empresa"></div>
      Se&ntilde;ores: <strong><?php echo $model->proveedor->razon_social; ?></strong><br>
      Atenci&oacute;n: <strong><?php echo $model->proveedor->representante; ?></strong><br>
      Ciudad.<br>
      Forma de Pagos: <strong>CREDITO</strong>
      <br>
      <div style="text-align:right; width: 100%">ORDEN DE COMPRA: <strong><?php echo $model->id; ?></strong></div>
      <br>
      <br>
      <table>
         <thead>
            <tr>
               <th>Cantidad</th>
               <th>Codigo</th>
               <th>Unidades</th>
               <th>Descripcion</th>
               <th>Precio</th>
               <th>Total</th>
            </tr>
         </thead>
         <tbody>
            <tbody>
            <?php $total=0; ?>
            <?php foreach ($model->ordenItems as $oi): ?>
            <tr>
               <td><?php echo $oi->cantidad; ?></td>
               <td><?php echo $oi->producto->id; ?></td>
               <td><?php echo $oi->producto->unidad->descripcion; ?></td>
               <td><?php echo $oi->producto->descripcion; ?></td>
               <td><?php echo $oi->precio_unitario; ?></td>
               <?php 
                  $subtotal  = $oi->cantidad * $oi->precio_unitario; 
                  $total    += $subtotal;
               ?>
               
               <td><?php echo $subtotal ; ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
               <td colspan="5">Sub-Total</td>
               <td><?php echo $subtotal; ?></td>
            </tr>
            <tr>
               <td colspan="5">I.V.A. 12%</td>
               <td><?php echo $total * 0.12; ?></td>
            </tr>
            <tr>
               <td colspan="5">Total a Pagar</td>
               <td><?php echo $total * 1.12; ?></td>
            </tr>
            </tfoot>
      </table>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <div class="encabezado_empresa"></div>
      Se&ntilde;ores: <strong><?php echo $model->proveedor->razon_social; ?></strong><br>
      Atenci&oacute;n: <strong><?php echo $model->proveedor->representante; ?></strong><br>
      Ciudad.<br>
      Forma de Pagos: <strong>CREDITO</strong>
      <br>
      <div style="text-align:right; width: 100%">ORDEN DE COMPRA: <strong><?php echo $model->id; ?></strong></div>
      <br>
      <br>
      <!--                        -->
      <table>
         <thead>
            <tr>
               <th>Cantidad</th>
               <th>Codigo</th>
               <th>Unidades</th>
               <th>Descripcion</th>
               <th>Precio</th>
               <th>Total</th>
            </tr>
         </thead>
         <tbody>
            <tbody>
            <?php $total=0; ?>
            <?php foreach ($model->ordenItems as $oi): ?>
            <tr>
               <td><?php echo $oi->cantidad; ?></td>
               <td><?php echo $oi->producto->id; ?></td>
               <td><?php echo $oi->producto->unidad->descripcion; ?></td>
               <td><?php echo $oi->producto->descripcion; ?></td>
               <td><?php echo $oi->precio_unitario; ?></td>
               <?php 
                  $subtotal  = $oi->cantidad * $oi->precio_unitario; 
                  $total    += $subtotal;
               ?>
               
               <td><?php echo $subtotal ; ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
               <td colspan="5">Sub-Total</td>
               <td><?php echo $subtotal; ?></td>
            </tr>
            <tr>
               <td colspan="5">I.V.A. 12%</td>
               <td><?php echo $total * 0.12; ?></td>
            </tr>
            <tr>
               <td colspan="5">Total a Pagar</td>
               <td><?php echo $total * 1.12; ?></td>
            </tr>
            </tfoot>
      </table>
   </body>
</html>
