<?php
/* @var $this UnidadController */
/* @var $model Unidad */

$this->breadcrumbs=array(
	'Unidads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Unidades', 'url'=>array('index')),
	array('label'=>'Manejar Unidades', 'url'=>array('admin')),
);
?>

<h1>Crear Unidad</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
