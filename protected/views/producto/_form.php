<?php
/* @var $this ProductoController */
/* @var $model Producto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'producto-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'precio_compra'); ?>
		<?php echo $form->textField($model,'precio_compra',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'precio_compra'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'producto_tipo_id'); ?>
		<?php echo $form->textField($model,'producto_tipo_id'); ?>
		<?php echo $form->error($model,'producto_tipo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unidad_id'); ?>
		<?php echo $form->textField($model,'unidad_id'); ?>
		<?php echo $form->error($model,'unidad_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->