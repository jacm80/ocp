<?php
/* @var $this ProductoController */
/* @var $data Producto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precio_compra')); ?>:</b>
	<?php echo CHtml::encode($data->precio_compra); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('producto_tipo_id')); ?>:</b>
	<?php echo CHtml::encode($data->productoTipo->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unidad_id')); ?>:</b>
	<?php echo CHtml::encode($data->unidad->descripcion); ?>
	<br />


</div>