<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Galletera Trigo de Oro</h1>

<h2>Nuestra Misi&oacute;n</h2>

<p>
   Galletera Trigo de Oro, C.A. fabrica galletas con 
   precios competitivos para el mercado venezolano, 
   aplicando para ello definidos estándares de calidad.
</p>

<h2>Nuestra Visi&oacute;n</h2>

<p>
   Alcanzar niveles de producción para asegurar la 
   expansión a través de nuestros distribuidores quienes 
   posicionan nuestros productos en cada estado del país, 
   llegando a cada uno de los puntos de ventas 
   (confiterías, supermercados, abastos y detallistas).
</p>
