<?php
	myCHtml::loadjQueryUI( );
	myCHtml::loadJs('compra'); 
?>
<style>
	h1 {
		margin: 0 auto;
		text-align:center;
	}
	table 
	{
		border: 1px solid black;
		width: 80%;
		margin: 0 auto;
	}
	th {
		background-color: #353877 !important;
		color: white;
	}
	th,td {
		border: 1px solid #e3e3e3;
	}
	tfoot tr td { text-align:right;} 
</style>
<?php
   $this->breadcrumbs=array('Compra'=>array('/compra'),'Create',);
?>
<h1>Registrar Compra</h1>
<br>

<form id="FrmCompra" action="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=compra/guardar" method="post">
<table>
   <tr>
      <td>Proveedor</td>
      <td><?php echo CHtml::dropDownList('Compra[proveedor_id]',0,Proveedor::getProveedores( ),array()); ?></td>
      <td>Fecha Factura</td>
      <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    								   'name'=>'Compra[fecha_emision]',
    								   // additional javascript options for the date picker plugin
    								   'options'=>array('showAnim'=>'fold'),
    								   'htmlOptions'=>array('style'=>'height:20px;')));?>
    	</td>
   </tr>
   <tr>
      <td>Numero</td>
      <td><input type="text" name="Compra[numero_factura]" id="numero_factura"/></td>
      <td>Fecha Vencimiento</td>
      <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    								   'name'=>'Compra[fecha_vencimiento]',
    								   // additional javascript options for the date picker plugin
    								   'options'=>array('showAnim'=>'fold'),
    								   'htmlOptions'=>array('style'=>'height:20px;')));?>

      </td>
   </tr>
</table>
<br>
<table>
   <thead>
		<th>Producto</th>
		<th>Cantidad</th>
		<th>Precio</th>
		<th>Monto</th>
		<th>&nbsp;</th>
	</thead>
   <tbody id="productos"></tbody>
	<tfoot>
      <tr>
			<td colspan="4">SubTotal</td>
			<td><div id="monto">0,00</div></td>
		</tr>
		<tr>
			<td colspan="4">IVA</td>
			<td><div id="iva">0,00</div></td>
		</tr>
		<tr>
			<td colspan="4">Total a Pagar</td>
			<td><div id="total">0,00</div></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align:center;">
				<button id="addProducto" type="button">Agregar Producto</button>
				<button id="guardarOrden" type="button">Guardar Compra</button>
				<button id="listarOrdenes" type="button">Listar Compras</button>
			</td>
		</tr>
	</tfoot>      
</table>
</form>
