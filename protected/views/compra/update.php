<?php
	myCHtml::loadjQueryUI( );
	myCHtml::loadJs('compra'); 
?>
<style>
	h1 {
		margin: 0 auto;
		text-align:center;
	}
	table 
	{
		border: 1px solid #e3e3e3;
		width: 80%;
		margin: 0 auto;
	}
	th {
		background-color: #353877 !important;
		color: white;
	}
	th,td {
		border: 1px solid #e3e3e3;
	}
   .moneda { text-align: right; }
   td input { width:80px; }
</style>
<?php
   $this->breadcrumbs=array('Compra'=>array('/compra'),'Create',);
?>
<h1>Registrar Compra</h1>
<br>

<form id="FrmCompra" action="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=compra/update/id/<?php echo $model->id; ?>" method="post">
<table>
   <tr>
      <td>Proveedor</td>
      <td><?php echo CHtml::dropDownList('Compra[proveedor_id]',$model->proveedor_id,Proveedor::getProveedores( ),array()); ?></td>
      <td>Fecha Factura</td>
      <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    								 'name' =>'Compra[fecha_emision]',
    								 'value'=> myCHtml::to_ddmmyyyy($model->fecha_emision),
    								 // additional javascript options for the date picker plugin
    								 'options'    =>array(
                                                'showAnim'=>'fold',
                                                'currentText'=>'Now',
                                                'dateFormat'=>'dd/mm/yy',
                                                ),
    								 'htmlOptions'=>array('style'=>'height:20px;')));?>
    	</td>
   </tr>
   <tr>
      <td>Numero</td>
      <td><?php echo CHtml::textField('Compra[numero_factura]',$model->numero_factura); ?></td>
      <td>Fecha Vencimiento</td>
      <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    								 'name' =>'Compra[fecha_vencimiento]',
    								 'value'=> myCHtml::to_ddmmyyyy($model->fecha_vencimiento),
    								 // additional javascript options for the date picker plugin
    								 'options'    =>array(
                                                'showAnim'=>'fold',
                                                'currentText'=>'Now',
                                                'dateFormat'=>'dd/mm/yy',
                                                ),
    								 'htmlOptions'=>array('style'=>'height:20px;')));?>

      </td>
   </tr>
   <tr>
      <td>Estatus</td>
      <td colspan="3">
         <?php 
            $estatusList = CHtml::listdata(EstatusCompra::model( )->findAll(),'id','descripcion');
            echo CHtml::radioButtonList('Compra[estatus_compra_id]',$model->estatus_compra_id,$estatusList); 
         ?>
      </td>
   </tr>
</table>
<br>
<table>
   <thead>
		<th>Producto</th>
		<th>Cantidad</th>
		<th>Precio</th>
		<th>Monto</th>
		<th>&nbsp;</th>
	</thead>
   <tbody id="productos">
   <?php $item=1; $base_imponible=0; ?>   
   <?php foreach($model->compraItems as $ci): ?>
      <tr id="<?php echo $item ?>">
				<td>
					<?php echo $ci->producto->descripcion; ?>
               <?php echo CHtml::hiddenField("item-$item",$ci->id); ?>               
               <?php echo CHtml::hiddenField('CompraItem[producto_id][]',$ci->producto_id); ?>
				</td>
				<td style="text-align:right">
               <?php echo CHtml::textField('CompraItem[cantidad][]',
                                          $ci->cantidad,
                                          array('class'=>'detalle cantidad','id'=>"cantidad-$item")); ?>
            </td>
				<td style="text-align:right">
               <?php echo CHtml::textField('CompraItem[precio_unitario][]',
                                           $ci->precio_unitario,
                                           array('class'=>'detalle precio','id'=>"precio-$item")); ?>
            </td>
            <td style="text-align:right">
               <div id="monto-<?php echo $item; ?>"><?php echo number_format($ci->cantidad * $ci->precio_unitario,2,'.',','); ?></div>
            </td>
				<td style="text-align:center">
               <img src="<?php echo Yii::app()->baseUrl; ?>/images/icons/cancel.png" 
                  style="cursor:pointer" class="eliminarItem" id="xitem-<?php echo $item ?>">
            </td>
			</tr>
      <?php $base_imponible+= ($ci->precio_unitario * $ci->cantidad); $item++; ?>      
   <?php endforeach; ?>
   </tbody>
	<tfoot>
      <tr>
			<td colspan="3" class="moneda">SubTotal</td>
			<td class="moneda"><div id="monto"><?php echo number_format($base_imponible,2,'.',','); ?></div></td>
         <td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" class="moneda">IVA</td>
			<td class="moneda"><div id="iva"><?php echo number_format($base_imponible * 0.12,'2','.',','); ?></div></td>
         <td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" class="moneda">Total a Pagar</td>
			<td class="moneda"><div id="total"><?php echo number_format($base_imponible*1.12,2,'.',','); ?></div></td>
         <td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" style="text-align:center;">
				<button id="addProducto" type="button">Agregar Producto</button>
				<button id="guardarCompra" type="button">Guardar</button>
				<button id="listarCompras" type="button">Listar</button>
			</td>
		</tr>
	</tfoot>      
</table>
</form>
