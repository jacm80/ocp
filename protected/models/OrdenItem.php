<?php

/**
 * This is the model class for table "orden_item".
 *
 * The followings are the available columns in table 'orden_item':
 * @property integer $id
 * @property integer $cantidad
 * @property string $precio_unitario
 * @property string $iva
 * @property integer $producto_id
 * @property integer $orden_compra_id
 *
 * The followings are the available model relations:
 * @property OrdenCompra $ordenCompra
 * @property Producto $producto
 */
class OrdenItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OrdenItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orden_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('producto_id, orden_compra_id', 'required'),
			array('cantidad, producto_id, orden_compra_id', 'numerical', 'integerOnly'=>true),
			array('precio_unitario, iva', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cantidad, precio_unitario, iva, producto_id, orden_compra_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ordenCompra' => array(self::BELONGS_TO, 'OrdenCompra', 'orden_compra_id'),
			'producto' => array(self::BELONGS_TO, 'Producto', 'producto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cantidad' => 'Cantidad',
			'precio_unitario' => 'Precio Unitario',
			'iva' => 'Iva',
			'producto_id' => 'Producto',
			'orden_compra_id' => 'Orden Compra',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('precio_unitario',$this->precio_unitario,true);
		$criteria->compare('iva',$this->iva,true);
		$criteria->compare('producto_id',$this->producto_id);
		$criteria->compare('orden_compra_id',$this->orden_compra_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}