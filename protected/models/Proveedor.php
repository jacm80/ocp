<?php

/**
 * This is the model class for table "proveedor".
 *
 * The followings are the available columns in table 'proveedor':
 * @property integer $id
 * @property string $razon_social
 * @property string $domicilio_fiscal
 * @property string $representante
 * @property string $telefono_fijo
 * @property string $telefono_movil
 *
 * The followings are the available model relations:
 * @property Compra[] $compras
 */
class Proveedor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Proveedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proveedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('telefono_fijo, telefono_movil', 'length', 'max'=>20),
			array('razon_social, domicilio_fiscal, representante', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, razon_social, domicilio_fiscal, representante, telefono_fijo, telefono_movil', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'compras' => array(self::HAS_MANY, 'Compra', 'proveedor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'razon_social' => 'Razon Social',
			'domicilio_fiscal' => 'Domicilio Fiscal',
			'representante' => 'Representante',
			'telefono_fijo' => 'Telefono Fijo',
			'telefono_movil' => 'Telefono Movil',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('razon_social',$this->razon_social,true);
		$criteria->compare('domicilio_fiscal',$this->domicilio_fiscal,true);
		$criteria->compare('representante',$this->representante,true);
		$criteria->compare('telefono_fijo',$this->telefono_fijo,true);
		$criteria->compare('telefono_movil',$this->telefono_movil,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	static function getProveedores( )
	{
		$data = array('0'=>'Seleccione');
		$proveedores = Proveedor::model( )->findAll( );
		foreach ($proveedores as $p)
		{
			$data[$p->id] = $p->razon_social;
		}
		return $data;
	}

}