<?php

/**
 * This is the model class for table "usuario".
 *
 * The followings are the available columns in table 'usuario':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $fecha_alta
 * @property integer $activo
 * @property integer $grupo_usuario_id
 *
 * The followings are the available model relations:
 * @property GrupoUsuario $grupoUsuario
 */


class Usuario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
   public $confirm_password;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username,password,confirm_password,grupo_usuario_id', 'required'),
			array('activo, grupo_usuario_id', 'numerical', 'integerOnly'=>true),
			array('username, password', 'length', 'max'=>255),
			array('fecha_alta', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, fecha_alta, activo, grupo_usuario_id', 'safe', 'on'=>'search'),
         array('password','compare','compareAttribute'=>'confirm_password')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'grupoUsuario' => array(self::BELONGS_TO, 'GrupoUsuario', 'grupo_usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'fecha_alta' => 'Fecha Alta',
			'activo' => 'Activo',
			'grupo_usuario_id' => 'Grupo Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('fecha_alta',$this->fecha_alta,true);
		$criteria->compare('activo',$this->activo);
		$criteria->compare('grupo_usuario_id',$this->grupo_usuario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
