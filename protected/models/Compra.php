<?php

/**
 * This is the model class for table "compra".
 *
 * The followings are the available columns in table 'compra':
 * @property integer $id
 * @property string $fecha_registro
 * @property string $numero_factura
 * @property string $fecha_emision
 * @property string $fecha_vencimiento
 * @property string $base_imponible
 * @property string $iva
 * @property string $estatus
 * @property integer $proveedor_id
 *
 * The followings are the available model relations:
 * @property Proveedor $proveedor
 * @property CompraItem[] $compraItems
 * @property CompraPago[] $compraPagos
 */
class Compra extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Compra the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'compra';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha_registro, proveedor_id', 'required'),
			array('proveedor_id', 'numerical', 'integerOnly'=>true),
			array('fecha_registro, numero_factura', 'length', 'max'=>45),
			array('base_imponible, iva', 'length', 'max'=>10),
			array('estatus', 'length', 'max'=>20),
			array('fecha_emision, fecha_vencimiento', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fecha_registro, numero_factura, fecha_emision, fecha_vencimiento, base_imponible, iva, estatus, proveedor_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proveedor' => array(self::BELONGS_TO, 'Proveedor', 'proveedor_id'),
			'compraItems' => array(self::HAS_MANY, 'CompraItem', 'compra_id'),
			'compraPagos' => array(self::HAS_MANY, 'CompraPago', 'compra_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fecha_registro' => 'Fecha Registro',
			'numero_factura' => 'Numero Factura',
			'fecha_emision' => 'Fecha Emision',
			'fecha_vencimiento' => 'Fecha Vencimiento',
			'base_imponible' => 'Base Imponible',
			'iva' => 'Iva',
			'estatus' => 'Estatus',
			'proveedor_id' => 'Proveedor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('numero_factura',$this->numero_factura,true);
		$criteria->compare('fecha_emision',$this->fecha_emision,true);
		$criteria->compare('fecha_vencimiento',$this->fecha_vencimiento,true);
		$criteria->compare('base_imponible',$this->base_imponible,true);
		$criteria->compare('iva',$this->iva,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('proveedor_id',$this->proveedor_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}