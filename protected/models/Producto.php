<?php

/**
 * This is the model class for table "producto".
 *
 * The followings are the available columns in table 'producto':
 * @property integer $id
 * @property string $descripcion
 * @property string $precio_compra
 * @property integer $producto_tipo_id
 * @property integer $unidad_id
 *
 * The followings are the available model relations:
 * @property CompraItem[] $compraItems
 * @property OrdenItem[] $ordenItems
 * @property Unidad $unidad
 * @property ProductoTipo $productoTipo
 */
class Producto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Producto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'producto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('producto_tipo_id, unidad_id', 'required'),
			array('producto_tipo_id, unidad_id', 'numerical', 'integerOnly'=>true),
			array('descripcion', 'length', 'max'=>45),
			array('precio_compra', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descripcion, precio_compra, producto_tipo_id, unidad_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'compraItems' => array(self::HAS_MANY, 'CompraItem', 'producto_id'),
			'ordenItems' => array(self::HAS_MANY, 'OrdenItem', 'producto_id'),
			'unidad' => array(self::BELONGS_TO, 'Unidad', 'unidad_id'),
			'productoTipo' => array(self::BELONGS_TO, 'ProductoTipo', 'producto_tipo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'precio_compra' => 'Precio Compra',
			'producto_tipo_id' => 'Producto Tipo',
			'unidad_id' => 'Unidad',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('precio_compra',$this->precio_compra,true);
		$criteria->compare('producto_tipo_id',$this->producto_tipo_id);
		$criteria->compare('unidad_id',$this->unidad_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	static function getProductos( )
	{
		$data = array('0'=>'Seleccione');
		$productos = Producto::model( )->findAll( );
		foreach ($productos as $p)
		{
			$data[$p->id] = $p->descripcion;
		}
		return $data;
	}

}