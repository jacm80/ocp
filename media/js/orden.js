var item=1;

function calcularTotal( ) {
   var monto=0;
   var iva=0;
   var total=0;
   var subtotal=0;


   var cantidad=0;
   var precio=0;
   $('.cantidad').each(function(i){
      n = $(this).prop('id').split('-')[1];      
      cantidad = $('#cantidad-'+n).val( );
      precio   = $('#precio-'+n).val( );
      monto    = cantidad * precio;
      subtotal += monto;
   });
   iva   = (subtotal * 0.12);
   total = (subtotal + iva);
   
   monto = monto.toFixed(2);
   iva   = iva.toFixed(2);
   total = total.toFixed(2);   

   $('#monto').html(monto);
   $('#iva').html(iva);
   $('#total').html(total);
}

function _chk_producto( ){
   var _valido = true; 
   $('.producto').each(function(i){
      if ($(this).prop('value') == 0) {
         _valido = false;
         return false;
      }
   });
   return _valido;
}

function _chk_cantidad( ){
   var _valido = true;
   $('.cantidad').each(function(i){
      if ($(this).prop('value') == '') {
         _valido=false;
         return false;
      }
   }); 
   return _valido;
}

function _chk_precio( ){
   var _valido = true;
   $('.precio').each(function(i){
      if ($(this).prop('value') == '') {
         _valido=false;
         return false;
      }
   }); 
   return _valido;
}


function _validar( ) {
   if  ($('#Orden_proveedor_id').val( )==0){
      alert('Seleccione el Proveedor');
      return false;
   }
   else if ($('#Orden_fecha_emision').val( )==''){
      alert('La fecha no puede quedar vacia');
      return false;
   }
   else if ($('#productos').children().size()==0){
      alert('Agregue el detalle');
      return false;
   }
   else if (! _chk_producto( ) ){
      alert('el producto no ha sido seleccionado');
      return false;
   }
   else if (! _chk_cantidad( )){
      alert('La cantidad no ha sido especificado');
      return false;
   }
   else if (! _chk_precio( )){
      alert('La cantidad no ha sido especificado');
      return false;
   }
   else return true;
}


$(document).ready(function(){
	
   $('#addProducto').click(function(){
		$.ajax({
			url: 'http://localhost/ocp/index.php?r=orden/addproducto',
			async: true,
			type:'get',
         data: { 'item': item },
			dataType: 'html',
			success: function(response){
				$('#productos').append(response);
            item++;
			},
			error: function(){
				alert('Error en el servidor');
			}
		});
	});

	$('#guardarOrden').click(function(){
      if (_validar( )){
         $('#FrmOrden').submit();
      }
	});

	$('#listarOrdenes').click(function(){
		location.href = "http://localhost/ocp/index.php?r=orden/index"; 
	});

   $('.detalle').live('change',function(){
      xid = $(this).prop('id').split('-')[1];
      //alert(xid);
      cantidad = $('#cantidad-'+xid);
      precio   = $('#precio-'+xid);
      monto    = $('#monto-'+xid);
      monto.html(cantidad.prop('value')*precio.prop('value'));
      calcularTotal( );
   });  

});
