var item=1;

/*
function calcularTotal( ) {
   var monto=0;
   var iva=0;
   var total=0;

   var cantidad=0;
   var precio=0;
   $('.cantidad').each(function(i){
      n = $(this).prop('id').split('-')[1];      
      cantidad += $('#cantidad-'+n).val( );
      precio   += $('#precio-'+n).val( );
      monto    += cantidad * precio;
   });
   iva   = (monto * 0.012);
   total = (monto + iva);
   
   monto = monto.toFixed(2);
   iva   = iva.toFixed(2);
   total = total.toFixed(2);   

   $('#monto').html(monto);
   $('#iva').html(iva);
   $('#total').html(total);
}
*/

function validar( ) {
   return true;
   /*
   if  ($('#Orden_proveedor_id').val( )==0)
   {
      alert('Seleccione el Proveedor');
      return false;
   }
   else if ($('#Orden_fecha_emision').val( )=='')
   {
      alert('La fecha no puede quedar vacia');
      return false;
   }
   else if ($('#productos').html( )=='')
   {
      alert('agregue el detalle');
      return false;
   }
   */
}


$(document).ready(function(){
	$('#addProducto').click(function(){
		$.ajax({
			url: 'http://localhost/ocp/index.php?r=compra/addproducto',
			async: true,
			type:'get',
         data: { 'item': item },
			dataType: 'html',
			success: function(response){
				$('#productos').append(response);
            item++;
			},
			error: function(){
				alert('Error en el servidor');
			}
		});
	});

	$('#guardarOrden').click(function(){
      if (validar( )){
         $('#FrmCompra').submit();
      }
	});

	$('#listarOrdenes').click(function(){
		location.href = "http://localhost/ocp/index.php?r=orden/index"; 
	});

   $('.detalle').live('change',function(){
      xid = $(this).prop('id').split('-')[1];
      //alert(xid);
      cantidad = $('#cantidad-'+xid);
      precio   = $('#precio-'+xid);
      monto    = $('#monto-'+xid);
      monto.html(cantidad.prop('value')*precio.prop('value'));
      calcularTotal( );
   });  

});
